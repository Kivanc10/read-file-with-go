package config

import (
	"errors"
	"fmt"
	"io/ioutil"
)

type HeaderError struct {
	FaultyHeader string
}

const fileHeader = "APPCONFIG"

var ErrnoConfigFile = errors.New("no config file does exist")

func (e *HeaderError) Error() string {
	return fmt.Sprintf("Bad header ; provided %s , expected : %s\n", e.FaultyHeader, fileHeader)
}

func Load() (string, error) {
	data, err := ioutil.ReadFile("/temp/config.txt")
	if err != nil {
		return "", ErrnoConfigFile
	}
	conf := string(data)

	if conf[0:8] != fileHeader {
		return "", &HeaderError{conf[0:8]}
	}

	return conf, nil

}
